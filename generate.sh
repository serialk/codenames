#!/bin/bash

# Got words.json inline in the javascript from https://codenames.game/
# Card tiles from https://codenames.game/img/card/

generate_words() {
    lang="$1"
    pack="$2"
    mkdir -p cards/"$lang/$pack"/
    cat words.json |
        jq --raw-output ".$lang.\"$pack\"[]" |
        xargs -P16 -n1 -I% \
        convert img/card-gray.png \
            -gravity Center -pointsize 115 -font img/FuturaPTCond-Bold.otf \
            -annotate +0+140 % \
            -fill '#B3987D' -annotate 180x180-120-70 % \
            -background white -alpha remove -flatten -alpha off \
            cards/"$lang/$pack"/'%'.png

    montage -geometry +0+0 cards/"$lang/$pack"/*.png \
        +compress -resize 198x126 -extent 198x126 \
        -tile 3x6 -gravity center -page a4 \
        "cards/$lang/$pack.pdf"
}

generate_identity() {
    montage -geometry +0+0 img/card-blue-back-*.png \
        +compress -resize 198x126 -extent 198x126 \
        -tile 3x6 -gravity center -page a4 \
        "cards/blue.pdf"

    montage -geometry +0+0 img/card-red-back-*.png \
        +compress -resize 198x126 -extent 198x126 \
        -tile 3x6 -gravity center -page a4 \
        "cards/red.pdf"

    montage -geometry +0+0 \
        img/card-gray-back.png -duplicate 16 \
        img/card-black-back.png \
        +compress -resize 198x126 -extent 198x126 \
        -tile 3x6 -gravity center -page a4 \
        "cards/other.pdf"
}

generate_identity
generate_words fr "Codenames"
generate_words fr "Codenames Duo"
